import 'package:flutter/material.dart';
import 'package:knowledgebase/Logic/MathInfoPage.dart';
import 'package:knowledgebase/Vocabulary/WordInfoPage.dart';
//import 'package:knowledgebuilder/Dialog/Dialogs.dart' as dlog;
//import 'package:knowledgebuilder/Logic/MathInfoPage.dart';
//import 'package:knowledgebuilder/Vocabulary/WordInfoPage.dart';


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: Text("Knowledge Builder"),
        ),
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          padding: EdgeInsets.only(left: 10, right: 10),
          children: <Widget>[

            Padding(
              padding: EdgeInsets.fromLTRB(40, 50, 40, 10),
              child: Material(
                borderRadius: BorderRadius.circular(15),
                shadowColor: Colors.purpleAccent,
                elevation: 5,
                child: MaterialButton(
                  height: 40,
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => WordInfoPage()),
                    );
                  },
//                  color: Colors.grey[200],
                  child: Text('Vocabulary Game', style: TextStyle(color: Colors.purple, fontSize: 20 )),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(40, 10, 40, 10),
              child: Material(
                borderRadius: BorderRadius.circular(15),
                shadowColor: Colors.purple,
                elevation: 7,
                child: MaterialButton(
                  height: 40,
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MathInfoPage()),
                    );
                  },
//                  color: Colors.purple,
                  child: Text('Logics Game', style: TextStyle(color: Colors.purple, fontSize: 20 )),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(40, 10, 40, 10),
              child: Material(
                borderRadius: BorderRadius.circular(15),
                shadowColor: Colors.purple,
                elevation: 5,
                child: MaterialButton(
                  height: 40,
//                  onPressed: (){
//                  Navigator.push(
//                    context,
//                    MaterialPageRoute(builder: (context) => MobileTopup()),
//                  );
//                },
                  color: Colors.grey[200],
                  child: Text('Help', style: TextStyle(color: Colors.purple, fontSize: 20 )),
                ),
              ),
            ),


          ],
        ),
      ),
    );

  }
}
