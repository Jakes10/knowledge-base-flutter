import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:knowledgebase/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.grey, // navigation bar color
        statusBarColor: Colors.grey[300]) );
    // status bar color
    return MaterialApp(
      title: 'Knowledgw Builder',

      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: Colors.purple

      ),
      home: Home(),
    );
  }

}


