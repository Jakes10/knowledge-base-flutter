import 'package:flutter/material.dart';
import 'package:knowledgebase/Vocabulary/WordGame.dart';
import 'package:knowledgebase/Vocabulary/WordInfoPage.dart';

class Successful extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SuccessfulState();
}

class SuccessfulState extends State<Successful>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 3000));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Padding(
            padding: const EdgeInsets.only(left: 40, right: 40, top: 410, bottom: 200),
            child: Container(
              decoration: ShapeDecoration(
                  color: Colors.grey[100],
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0))),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child:Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [

                        IconButton(
                          icon: Icon(Icons.ac_unit,  color: Colors.purple, size: 32,),

                        ),

                        Text("Want to play again?", style: TextStyle(fontSize: 15),),


                      ],


                    ),
                    SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Material(
                          borderRadius: BorderRadius.circular(15),
                          shadowColor: Colors.black,
                          elevation: 6,
                          color: Colors.white,
                          child: MaterialButton(

                            height: 40,
                            onPressed: ()=>Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => WordInfoPage()),
                            ),
                            child: Text('No',
                                style: TextStyle(color: Colors.purple[400], fontSize: 18)),
                          ),
                        ),
                        SizedBox(width: 10,),

                        Material(
                          borderRadius: BorderRadius.circular(15),
                          shadowColor: Colors.black,
                          elevation: 2,
                          color: Colors.purple[400],
                          child: MaterialButton(

                            height: 40,
                            onPressed: ()=>Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => WordGame()),
                            ),
                            child: Text('Yes',
                                style: TextStyle(color: Colors.white, fontSize: 18)),
                          ),
                        ),
                      ],
                    )
                  ],
                ),

              ),
            ),
          ),
        ),
      ),
    );
  }
}