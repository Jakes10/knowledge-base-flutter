import 'package:flutter/material.dart';
import 'package:knowledgebase/Logic/MathGame.dart';

class MathInfoPage extends StatefulWidget {
  @override
  _MathInfoPageState createState() => _MathInfoPageState();
}

class _MathInfoPageState extends State<MathInfoPage> {
  String gameInfo = "Your opponent will choose a number and the operation boxes will be given based on the difficulty level. "
      "Each operation box will be input for numbers and you will select appropiate operators to get it equal to the number that your opponent chose. "
      "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text("Math & Logic"),
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          padding: EdgeInsets.only(left: 10, right: 10, top: 20),
          children: <Widget>[


            Padding(
              padding: const EdgeInsets.all(20.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                  Expanded(
                    child: Text(gameInfo, style: TextStyle(fontSize: 15)),
                  ),
                ],


              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(80, 10, 80, 10),
              child: Material(
                borderRadius: BorderRadius.circular(15),
                shadowColor: Colors.purpleAccent,
                elevation: 5,
                child: MaterialButton(
                  height: 40,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MathGame()),
                    );
                  },
//                  color: Colors.grey[200],
                  child: Text('Let\'s Go!!',
                      style: TextStyle(color: Colors.purple, fontSize: 20)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}