import 'package:http/http.dart' as http;

class ApiConnection{
  final String url="https://od-api.oxforddictionaries.com/api/v2/entries/en-gb/";



  postData(data, apiUrl) async{
    var fulUrl= url+apiUrl;

    try {
      return http.post(
          fulUrl,
          body: data,
          headers: _setHeaders()
      );
    } catch (e) {
      print(e);
      return null;
    }

  }

  getData(apiUrl) async{


    var fulUrl= url+apiUrl;
//    print(url+apiUrl);
    try {
      return http.get(
          fulUrl,
          headers: _setHeaders()
      );

    } catch (e) {
      print(e);
      return null;
    }

  }

  _setHeaders()=>{
    "Accept": "application/json",
    "app_id": "7a948237",
    "app_key": "cbf4f35b88e6542f9305e17422877a9c"
  };

}












