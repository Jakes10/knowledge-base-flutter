import 'package:flutter/material.dart';
import 'package:knowledgebase/Vocabulary/WordGame.dart';

class WordInfoPage extends StatefulWidget {
  @override
  _WordInfoPageState createState() => _WordInfoPageState();
}

class _WordInfoPageState extends State<WordInfoPage> {
  String gameInfo="Each player's objective is to think of a word he\\she wants to generate and try to be the first to complete that word. "
      "If your opponent change your word to something else, you must either complete that word or changes it to another. "
      "The more sophisticated the word is the more points you will gain. The First player to score 100 points wins the game!!";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text("Knowledge Builder"),
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          padding: EdgeInsets.only(left: 10, right: 10, top: 20),
          children: <Widget>[


            Padding(
              padding: const EdgeInsets.all(20.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                  Expanded(
                    child: Text(gameInfo, style: TextStyle(fontSize: 15)),
                  ),
                ],


              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(80, 10, 80, 10),
              child: Material(
                borderRadius: BorderRadius.circular(15),
                shadowColor: Colors.purpleAccent,
                elevation: 5,
                child: MaterialButton(
                  height: 40,

                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => WordGame()),
                    );
                  },
//                  color: Colors.grey[200],
                  child: Text('Let\'s Go!!', style: TextStyle(color: Colors.purple, fontSize: 20 )),
                ),
              ),
            ),
          ],
        ),
      ),
    );

  }
}
