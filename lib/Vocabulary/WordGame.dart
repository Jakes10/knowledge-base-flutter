//import 'dart:html';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledgebase/API/ApiConnection.dart';
import 'package:knowledgebase/Data/Player.dart';
import 'dart:math';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:knowledgebase/Dialog/successful.dart';


class WordGame extends StatefulWidget {
  @override
  _WordGameState createState() => _WordGameState();
}

class _WordGameState extends State<WordGame> {
  String word ='';
  String definition='';
  int player1Index=0;
  int player2Index=0;
  var alpha = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
  var errors = ['Oops! That is not a word..',
    'No way this word exist!',
    'Seriously?',
    'I know you can do better than that!',
    'HUH?',
    'SMH! What the heck?',
    'LOL!!',
    'Sorry, better luck next time!',
  ];

  bool player1BtnDisabled=false;
  bool player2BtnDisabled=false;
  bool player1Surrendered=false;
  bool player2Surrendered=false;
  bool claimed=false;
  bool player1won=false;
  bool player2won=false;
  bool noGivingUp=false;
  Player player1= new Player();
  Player player2= new Player();
  var giveUp=0;
  var award=0;
  String player2PointsGain="";
  String player1PointsGain="";
  bool isAWord=true;
  DateTime current;


  @override
  void initState() {
    // TODO: implement initState
    player1.score=0;
    player2.score=0;
    super.initState();
  }

  void checkForWinner(){
    if(player2.score>=100){
      player2won=true;
      showDialog(
        context: context,
        builder: (_) => Successful(),
      );
    }


    if(player1.score>=100){
      player1won=true;
      showDialog(
        context: context,
        builder: (_) => Successful(),
      );

    }
  }
  Future<bool> getWordMeaning() async {
    var response = await new ApiConnection().getData(word+"?fields=definitions&strictMatch=false");

    if (response.statusCode == 200) {
      var jsonDecode = json.decode(response.body);
      var def=jsonDecode["results"][0]["lexicalEntries"][0]["entries"][0]["senses"][0]["definitions"];
      setState(() {
        definition= def.toString().replaceAll("[","").replaceAll("]","");
        isAWord=true;
      });
    }else{

      setState(() {

        definition= errors[0 + new Random().nextInt(7 - -1)];//display error messages
        isAWord=false;
      });
    }

    return isAWord;
  }

  Future<void> getCompoundWord(int player) async {
    var response = await new ApiConnection().getData(word+"?fields=definitions&strictMatch=false");

    if (response.statusCode == 200) {
      var jsonDecode = json.decode(response.body);
      var def=jsonDecode["results"][0]["lexicalEntries"][0]["entries"][0]["senses"][0]["definitions"];
      setState(() {
        definition= def.toString().replaceAll("[","").replaceAll("]","");
        isAWord=true;
        if(player==1){
          player1.score= player1.score + int.parse((word.length*0.60).toStringAsFixed(0));
          player1PointsGain="+"+(word.length*0.60).toStringAsFixed(0);
        }
        else{
          player2.score= player1.score + int.parse((word.length*0.60).toStringAsFixed(0));
          player2PointsGain="+"+(word.length*0.60).toStringAsFixed(0);

        }
        award= int.parse((word.length*0.40).toStringAsFixed(0));

      });
    }else{

      setState(() {

        isAWord=false;
      });
    }

  }

  void surrender(int player){
    setState(() {
      if(giveUp!=0){
        noGivingUp=true;
        if(player==1){

          player1Surrendered=true;
          player2BtnDisabled=false;

          player1.score=player1.score-giveUp;
          player2.score=player2.score+giveUp;
          player1PointsGain="-"+giveUp.toString();
          player2PointsGain="+"+giveUp.toString();

          giveUp=0;
        }

        if(player==2){

          player2Surrendered=true;
          player1BtnDisabled=false;

          player1.score=player1.score+giveUp;
          player2.score=player2.score-giveUp;
          player1PointsGain="+"+giveUp.toString();
          player2PointsGain="-"+giveUp.toString();

          giveUp=0;
        }
        checkForWinner();
      }

      if(player1.score<0)
        player1.score=0;
      if(player2.score<0)
        player2.score=0;

    });
  }

  void turnOverPlay(int player){
    if(player==1){
      player1BtnDisabled=true;
      player2BtnDisabled=false;

    }else{
      player2BtnDisabled=true;
      player1BtnDisabled=false;
    }
  }

  void claimAward(int player) async{

    if(word.length>3){

      claimed=true;
      player2Surrendered=false;
      player1Surrendered=false;

      isAWord= await getWordMeaning();
      noGivingUp=false;
      if (player == 1 ) {
        turnOverPlay(1);

        if(isAWord){

          player1PointsGain="+"+award.toString();

          player1.score= player1.score+award;
          word="";
        }else{
          player2.score= player2.score+award;
          player2PointsGain="+"+award.toString();
          player1PointsGain="0";

        }

      }

      if (player == 2 ) {
        //Turn over the play t0 the opponent

        turnOverPlay(2);

        if(isAWord){//Gives the opponent the points if the result was an actual word
          player2.score= player2.score+award;
          player2PointsGain="+"+award.toString();
          word="";
        }else{
          player1.score= player1.score+award;
          player1PointsGain="+"+award.toString();
          player2PointsGain="0";
        }
      }
      checkForWinner();
    }
    award=0;
    giveUp=0;


  }

  void selectLetter(int player){

    setState(() {
      if(claimed) word="";

      if(definition.isNotEmpty) definition="";

      claimed=false;

      if(player==1){

        if(player2Surrendered==false) turnOverPlay(1);

        word=word+alpha[player1Index];
        if(word.length>3){
          award= int.parse((word.length*0.90).toStringAsFixed(0));
          giveUp= int.parse((word.length*0.25).toStringAsFixed(0));
          getCompoundWord(1);

        }
        if(player1Surrendered ) player2BtnDisabled =false;

      }else{

        word = word + alpha[player2Index]; //Add selected letter to the unknown word

        if(player1Surrendered==false) turnOverPlay(2);

        if(word.length>3){
          award= int.parse((word.length*0.90).toStringAsFixed(0));
          giveUp= int.parse((word.length*0.25).toStringAsFixed(0));

          getCompoundWord(2);
        }

        if(player2Surrendered) player1BtnDisabled =false;

      }

      if(definition.isEmpty){
        player1PointsGain="";
        player2PointsGain="";
      }
//
    });

  }

  Future<bool> backButtonPressed() {
    DateTime now = DateTime.now();

    if(current == null || now.difference(current)> Duration(seconds: 2)){
      current=now;
      Fluttertoast.showToast(
        toastLength: Toast.LENGTH_SHORT,
        msg: "Press again to exit game!",

      );
      return Future.value(false);
    }else{
      return Future.value(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()=>backButtonPressed(),
      child:Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: <Widget>[
            Expanded(
                child:  RotationTransition(
                  turns: new AlwaysStoppedAnimation(180/360),
                  child: Container(
                    child: ListView(
                      children: <Widget>[
                        Card(
                          margin: EdgeInsets.symmetric(vertical: 0.0),
                          color: Colors.blueGrey[50],
                          elevation: 0,
                          child: ListTile(
                            title: Text(word),
                            subtitle: Text(definition),
                          ),
                        ),
                        Container(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.height - (MediaQuery.of(context).size.height/1.22),
                              ),
                              Container(
                                child: new Center(
                                    child: Container(
                                        width: 50,
                                        height: 120,
                                        child: CupertinoPicker(
                                          backgroundColor: Colors.transparent,
                                          itemExtent: 50,
                                          onSelectedItemChanged: (int index){
//                                          print(index);
                                            setState(() {
                                              player2Index=index;
                                            });
                                          },
                                          children: List<Widget>.generate(26, (int index) {
                                            return Padding(
                                              padding: const EdgeInsets.only(top: 15 ),
                                              child: Text(alpha[index], style: TextStyle(fontSize: 16, color: Colors.purple)),
                                            );
                                          }),
                                        )
                                    )
                                ),
                              ),
                              SizedBox(width: 50,),
                              Offstage(
                                offstage: false,
//                              offstage: player2Surrendered,
                                child: AbsorbPointer(
                                  absorbing: player2Surrendered ? player2Surrendered : player2BtnDisabled ,
//                                absorbing: player2BtnDisabled ,
                                  child: Material(
                                    borderRadius: BorderRadius.circular(15),
                                    shadowColor: Colors.black,
                                    elevation: 2,
                                    color: Colors.purple[400],
                                    child: MaterialButton(
                                      height: 40,
                                      onPressed: ()=>selectLetter(2),
                                      child: Text('Select',
                                          style: TextStyle(color: Colors.white, fontSize: 18)),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                AbsorbPointer(
                                  absorbing: player2Surrendered? player2Surrendered : player2BtnDisabled ,
//                                absorbing: player2BtnDisabled ,
                                  child: Offstage(
                                    offstage: noGivingUp,
                                    child: Material(
                                      borderRadius: BorderRadius.circular(15),
                                      shadowColor: Colors.purple,
                                      elevation: 5,
                                      color: Colors.white,
                                      child: MaterialButton(
                                        height: 40,
                                        onPressed: ()=>surrender(2),
                                        child: Text('Give up '+giveUp.toString(),
                                            style: TextStyle(color: Colors.purple, fontSize: 18)),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 40,),
                                AbsorbPointer(
                                  absorbing: player2Surrendered? player2Surrendered : player2BtnDisabled ,
                                  child: Material(
                                    borderRadius: BorderRadius.circular(15),
                                    shadowColor: Colors.purple,
                                    elevation: 5,
                                    color: Colors.white,
                                    child: MaterialButton(
                                      height: 40,
                                      onPressed: ()=>claimAward(2),
                                      child: Text('Claim '+ award.toString(),
                                          style: TextStyle(color: Colors.purple, fontSize: 18)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                )
            ),
            Row(mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Expanded(
                  child: Column(
//                  crossAxisAlignment: CrossAxisAlignment.end,
//                  mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          RotationTransition(
                            turns: new AlwaysStoppedAnimation(180/360),
                            child: player2won? Padding(
                              padding: const EdgeInsets.only(right: 30, bottom: 20),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.check_circle, color: Colors.green[600],),
                                  SizedBox(width: 5,),
                                  new Text("You Won!!", style: TextStyle(fontSize: 18, color: Colors.green[600])),
                                ],
                              ),
                            ):Padding(
                              padding: const EdgeInsets.only(right: 10, bottom: 20),
                              child: player1won? Row(
                                children: <Widget>[
                                  Icon(Icons.cancel, color: Colors.red[600],),
                                  SizedBox(width: 5,),
                                  new Text("You Lost!!",  style: TextStyle(fontSize: 18, color: Colors.red[900])),
                                ],
                              ):new Text(""),
                            ),
                          ),
                          SizedBox(width: MediaQuery.of(context).size.width*0.20,),
                          RotationTransition(
                              turns: new AlwaysStoppedAnimation(180/360),
                              child: Padding(
                                  padding: const EdgeInsets.only( left: 30),
                                  child: player2PointsGain.contains("+") ? new Text(player2PointsGain, style: TextStyle(fontSize: 16, color: Colors.green[600]),):
                                  new Text(player2PointsGain, style: TextStyle(fontSize: 16, color: Colors.red[600]),))
                          ),
                          RotationTransition(
                            turns: new AlwaysStoppedAnimation(90/360),
                            child: Padding(
                              padding: const EdgeInsets.only(right: 30, top: 20),
                              child: new Text(player2.score.toString(), style: TextStyle(fontSize: 16, color: Colors.purple[600]),),
                            ),
                          ),
                        ],
                      ),
                      Column(children: <Widget>[Divider(height: 12,thickness: 2,color: Colors.purple,)],),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only( bottom: 20, left: 30),
                            child: player1won? Padding(
                              padding: const EdgeInsets.only(right: 30, bottom: 20),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.check_circle, color: Colors.green[600],),
                                  SizedBox(width: 5,),
                                  new Text("You Won!!", style: TextStyle(fontSize: 18, color: Colors.green[600])),
                                ],
                              ),
                            ):Padding(
                              padding: const EdgeInsets.only(right: 30, bottom: 20),
                              child: player2won?  Row(
                                children: <Widget>[
                                  Icon(Icons.cancel, color: Colors.red[600],),
                                  SizedBox(width: 5,),
                                  new Text("You Lost!!",  style: TextStyle(fontSize: 18, color: Colors.red[900])),
                                ],
                              ):new Text(""),
                            ),
                          ),
//                        ),
                          SizedBox(width: MediaQuery.of(context).size.width*0.20,),
                          Padding(
                              padding: const EdgeInsets.only(right: 40,  bottom: 20),
                              child: player1PointsGain.contains("+") ? new Text(player1PointsGain, style: TextStyle(fontSize: 16, color: Colors.green[600]),):
                              new Text(player1PointsGain, style: TextStyle(fontSize: 16, color: Colors.red[600]),)
                          ),
                          RotationTransition(
                            turns: new AlwaysStoppedAnimation(90/360),
                            child: Padding(
                              padding: const EdgeInsets.only(  left: 20, top: 30 ),
                              child: new Text(player1.score.toString(), style: TextStyle(fontSize: 16, color: Colors.purple[600]),),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),

            Expanded(
                child:  Container(
                  child: ListView(
                    children: <Widget>[
                      Card(
                        margin: EdgeInsets.symmetric(vertical: 0.0),
                        color: Colors.blueGrey[50],
                        elevation: 0,
                        child: ListTile(
                          title: Text(word),
                          subtitle: Text(definition),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.height - (MediaQuery.of(context).size.height/1.22),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center ,
                              children: <Widget>[
                                Container(
                                  child: Container(
                                      width: 50,
                                      height: 120,
                                      child: CupertinoPicker(
                                        backgroundColor: Colors.transparent,
                                        itemExtent: 50,
                                        onSelectedItemChanged: (int index){
                                          setState(() {
                                            player1Index=index;//Maintaining position of previous word selected
                                          });
                                        },
                                        children: List<Widget>.generate(26, (int index) {
                                          return Padding(
                                            padding: const EdgeInsets.only(top: 15, left: 5),
                                            child: Text(alpha[index], style: TextStyle(fontSize: 16, color: Colors.purple)),
                                          );
                                        }),
                                      )
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(width: 50,),
                            Offstage(
                              offstage: false,
                              child: AbsorbPointer(
                                absorbing: player1Surrendered? player1Surrendered : player1BtnDisabled ,
//                                absorbing: player1BtnDisabled ,
                                child: Material(
                                  borderRadius: BorderRadius.circular(15),
                                  shadowColor: Colors.black,
                                  elevation: 2,
                                  color: Colors.purple[400],
                                  child: MaterialButton(
                                    height: 40,
                                    onPressed: ()=>selectLetter(1),
                                    child: Text('Select',
                                        style: TextStyle(color: Colors.white, fontSize: 18)),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                      Offstage(
                        offstage: false,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Offstage(
                                  offstage: noGivingUp,
                                  child: AbsorbPointer(
                                    absorbing: player1Surrendered ? player1Surrendered : player1BtnDisabled ,
                                    child: Material(
                                      borderRadius: BorderRadius.circular(15),
                                      shadowColor: Colors.purple,
                                      elevation: 5,
                                      color: Colors.white,
                                      child: MaterialButton(
                                        height: 40,
                                        onPressed: ()=>surrender(1),
                                        child: Text('Give up '+giveUp.toString(),
                                            style: TextStyle(color: Colors.purple, fontSize: 18)),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 40,),
                                AbsorbPointer(
                                  absorbing: player1Surrendered ? player1Surrendered : player1BtnDisabled ,
//                                absorbing: player1BtnDisabled ,
                                  child: Material(
                                    borderRadius: BorderRadius.circular(15),
                                    shadowColor: Colors.purple,
                                    elevation: 5,
                                    color: Colors.white,
                                    child: MaterialButton(
                                      height: 40,
                                      onPressed: ()=>claimAward(1),
                                      child: Text('Claim '+ award.toString(),
                                          style: TextStyle(color: Colors.purple, fontSize: 18)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
            ),
          ],
        ),
      )
    );
  }
}



